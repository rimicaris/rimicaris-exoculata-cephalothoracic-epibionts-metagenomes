# Rimicaris Exoculata Cephalothoracic Epibionts Metagenomes

This project describes the reproducible bioinformatics workflow for our study titled “Niche Partitioning in the Rimicaris Exoculata Holobiont: The Case of the First Symbiotic Zetaproteobacteria”.

### Downloading individal epibiont metagenomes

The raw Illumina paired-end sequencing files for shotgun metagenomes are stored in European Nucleotide Archive under the accession number PRJEB37577, and the following table lists the individual accession numbers for each sample:

| Sample accession | sample name in our study |
|:----------------:|:------------------------:|
| ERS4641946       | RE12                     |
| ERS4641947       | RE13                     |
| ERS4641942       | RE3                      |
| ERS4641943       | RE7                      |
| ERS4641945       | RE5                      |
| ERS4641944       | RE6                      |

If you wish to follow this workflow, you can download raw Illumina paired-end sequencing data file into your work directory using the following script:

[download_data.sh](files/download_samples.sh)

Once your downloads are finished, you must have the following files in your work directory:

```
- RE3_RAW_R1.fastq.gz, RE3_RAW_R2.fastq.gz
- RE7_RAW_R1.fastq.gz, RE7_RAW_R2.fastq.gz
- RE5_RAW_R1.fastq.gz, RE5_RAW_R2.fastq.gz
- RE6_RAW_R1.fastq.gz, RE6_RAW_R2.fastq.gz
- RE12_RAW_R1.fastq.gz, RE12_RAW_R2.fastq.gz
- RE13_RAW_R1.fastq.gz, RE13_RAW_R2.fastq.gz
```

### Adaptor trimming

We downloaded the truseq adapters sequences for bbduk,
```
wget https://github.com/BioInfoTools/BBMap/raw/master/resources/truseq.fa.gz
```
and ran the following command in our work directory to remove the illumina adaptors from our sequences:

```
for r1 in *_R1.fastq.gz
do
# copy the filename, r1, to a new file name, r2 and substitute R1 in the name with R2
# this generates the name of the R2 file
    r2=$r1
    r2="${r1/_R1/_R2}"
# generate the names for the output files
    r1t=$r1
    r1t="${r1t/_R1.fastq.gz/_trimmed_R1.fastq.gz}"
    r2t=$r1
    r2t="${r2t/_R1.fastq.gz/_trimmed_R2.fastq.gz}"
    NAME=$r1
    NAME="${NAME/_R1.fastq.gz/}"
# run the bbduk command
     bbduk.sh in1=$r1 in2=$r2 \
              out1=$r1t out2=$r2t \
              ref=truseq.fa.gz \
              stats="$NAME"_stats.txt \
              ktrim=r k=23 mink=11 hdist=1 tpe tbo 
done	
```
### Running the anvi'o metagenomic workflow

We used the anvi'o metagenomic workflow to perform the following step

* Quality filter short reads using illumina-utils,
* Coassemble samples by site using Megahit,
* Recruit reads from all samples using each independent assembly with Bowtie2,
* Profile and merge resulting files using anvi’o.

We ran the workflow the following way:

```
anvi-run-workflow -w metagenomics \
                  -c config-01_megahit.json \
                  --additional-params \
                  --cores 12 \
                  --keep-going --rerun-incomplete
```

The config file and the samples files used in the workflow are available this way:

[samples.txt](files/samples.txt)

[config-01_megahit.json](files/config-01_megahit.json)

### Taking a look at the output files

Successful completion of the anvi’o metagenomic workflow results in various standard output directories, including `00_LOGS`, `01_QC`, `02_FASTA`, `03_CONTIGS`, `04_MAPPING`, `05_ANVIO_PROFILE`, and `06_MERGED`. Below is a brief summary of the contents of these directories:

* `00_LOGS`: Log files for every operation.

* `01_QC`: Quality-filtered short metagenomic reads and final statistics.

* `02_FASTA`: Megahit coassembly of each site in the form of FASTA files.

* `03_CONTIGS`: Anvi’o contigs databases for each assembly with HMM hits for single-copy core genes.

```
RB-contigs.db	SP-contigs.db	TAG-contigs.db
```
* `04_MAPPING`: Bowtie2 read recruitment results for coassembly outputs from quality-filtered short metagenomic reads in the form of BAM files.

```
ls 04_MAPPING/
RB  SP  TAG
```
```
ls 04_MAPPING/RB:
RB-contigs.1.bt2  RB-contigs.4.bt2      RE12.bam      RE13.bam.bai  RE5.bam      RE6.bam.bai
RB-contigs.2.bt2  RB-contigs.rev.1.bt2  RE12.bam.bai  RE3.bam       RE5.bam.bai  RE7.bam
RB-contigs.3.bt2  RB-contigs.rev.2.bt2  RE13.bam      RE3.bam.bai   RE6.bam      RE7.bam.bai
```

* `05_ANVIO_PROFILE`: Anvi’o single profiles for each sample.

```
ls 05_ANVIO_PROFILE/
RB  SP  TAG
```
```
ls -R 05_ANVIO_PROFILE/RB/
05_ANVIO_PROFILE/RB/:
RE12  RE13  RE3  RE5  RE6  RE7

05_ANVIO_PROFILE/RB/RE12:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/RB/RE13:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/RB/RE3:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/RB/RE5:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/RB/RE6:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/RB/RE7:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt
```

* `06_MERGED`: Anvi’o merged profile databases for each coassembly.

```
ls -R 06_MERGED/

06_MERGED/:
RB  SP  TAG

06_MERGED/RB:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/SP:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt:

06_MERGED/TAG:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt
```
### Reconstructing Rimicaris exoculata cephalothoracic epibiotic community genomes

To perform the next commands in each coassembly set we used the set.txt file which lists the three coassembly set

```
cat set.txt
RB
SP
TAG
```
To facilitate the genomes reconstruction we added single-copy core genes taxonomy determined by the Genome Taxonomy Database (GTDB) with the following program:

```
for NAME in `cat set.txt`
do
    anvi-run-scg-taxonomy -c 03_CONTIGS/$NAME-contigs.db -T 6 -P 6 
done
```
and we added iron genes function with the following program:

```
mkdir 07_FEGENIE

for NAME in `cat set.txt`
do
    # Get genes aa sequences et remove empty sequences
    anvi-get-sequences-for-gene-calls -c 03_CONTIGS/$NAME-contigs.db \
                                      --get-aa-sequences \
                                      -o 03_CONTIGS/$NAME-protein-sequences.fa
    sed -i 's/>/>genecall_/' 03_CONTIGS/$NAME-protein-sequences.fa
    sed -i '/^>/ {N; /\n$/d}' 03_CONTIGS/$NAME-protein-sequences.fa

    # Run fegenie
    FeGenie.py -bin_dir 03_CONTIGS \
               --orfs -bin_ext fa -t 16 \
               -out 07_FEGENIE/$NAME

    # Parse functions output
    # Make gene call table for each group
    echo -e "gene_callers_id\tsource\taccession\tfunction\te_value" \
         >> 07_FEGENIE/$NAME/FeGenie-geneSummary_parsed.txt
    tail -n +3 07_FEGENIE/$NAME/FeGenie-geneSummary.csv | while read line
    do
        FUNCTION=`echo $line | awk -F"," '{print $4}'`
        GENE=`echo $line | awk -F"," '{print $3}' | sed -e 's/genecall_//g'`
        echo -e $GENE"\tFeGenie\t\t"$FUNCTION"\t0" >> 07_FEGENIE/$NAME/FeGenie-geneSummary_parsed.txt
    done

    # Import fegenie functions in anvio
    anvi-import-functions -c 03_CONTIGS/$NAME-contigs.db \
                          -i 07_FEGENIE/$NAME/FeGenie-geneSummary_parsed.txt \

    # Parse categories output
    # Make gene call table for each group
    echo -e "gene_callers_id\tsource\taccession\tfunction\te_value" \
         >> 07_FEGENIE/$NAME/FeGenie-geneSummary_category_parsed.txt
    tail -n +3 07_FEGENIE/$NAME/FeGenie-geneSummary.csv | while read line
    do
        FUNCTION=`echo $line | awk -F"," '{print $1}'` 
        GENE=`echo $line | awk -F"," '{print $3}' | sed -e 's/genecall_//g'`
        echo -e $GENE"\tFeGenie_category\t\t"$FUNCTION"\t0" \
             >> 07_FEGENIE/$NAME/FeGenie-geneSummary_category_parsed.txt
    done

    # Import fegenie categories in anvio
    anvi-import-functions -c 03_CONTIGS/$NAME-contigs.db \
                          -i 07_FEGENIE/$NAME/FeGenie-geneSummary_category_parsed.txt \
done

```
To reconstruct population genomes we automatically divided all contigs into half of the estimated genome number found with the single-copy core genes.

We used `anvi-display-contigs-stats` to obtain the estimated genome number. Running the following command in our `03_CONTIGS` directory,

```
anvi-display-contigs-stats *-contigs.db --report-as-text --output-file CONTIGS-STATS.txt
```
gave us a TAB separated file comparing contigs statistics including the estimated genome number of each coassembly:

| contigs_db                  | RB        | SP       | TAG       |
|:---------------------------:|:---------:|:--------:|:---------:|
| Total Length                | 184824058 | 57957494 | 180499708 |
| Num Contigs                 | 53608     | 34823    | 64689     |
| Num Genes (prodigal)        | 199048    | 87675    | 200585    |
| Longest Contig              | 173185    | 15661    | 110444    |
| Shortest Contig             | 1000      | 1000     | 1000      |
| N50                         | 7002      | 11516    | 10338     |
| N75                         | 20624     | 21730    | 28763     |
| N90                         | 37287     | 29246    | 48021     |
| L50                         | 5660      | 1650     | 3757      |
| L75                         | 2194      | 1244     | 1722      |
| L90                         | 1308      | 1082     | 1187      |
| Campbell_et_al              | 8036      | 2330     | 7264      |
| BUSCO_83_Protista           | 200       | 54       | 159       |
| Ribosomal_RNAs              | 46        | 10       | 62        |
| Rinke_et_al                 | 4941      | 1488     | 4575      |
| bacteria (Campbell_et_al)   | 55        | 14       | 52        |
| eukarya (BUSCO_83_Protista) | 0         | 0        | 0         |
| archaea (Rinke_et_al)       | 0         | 0        | 0         |

We ran CONCOCT to generate bins corresponding to half of the estimated genome number in our merged profile database:

```
anvi-cluster-with-concoct -c 03_CONTIGS/RB-contigs.db -p 06_MERGED/RB/PROFILE.db --num-clusters 27 -C CONCOCT
anvi-cluster-with-concoct -c 03_CONTIGS/SP-contigs.db -p 06_MERGED/SP/PROFILE.db --num-clusters 7 -C CONCOCT 
anvi-cluster-with-concoct -c 03_CONTIGS/TAG-contigs.db -p 06_MERGED/TAG/PROFILE.db --num-clusters 26 -C CONCOCT
```

We used `anvi-interactive` and `anvi-refine` to manually refine the three CONCOCT bins collections using the anvio interactive interface:

```
anvi-interactive -c 03_CONTIGS/RB-contigs.db \
                 -p 06_MERGED/RB/PROFILE.db
```

```
anvi-refine -c 03_CONTIGS/RB-contigs.db \
            -p 06_MERGED/RB/PROFILE.db \
            -C CONCOCT -b Bin_1
```

### Identification and removal of redundant MAGs

After completion of the refinement, we used the program anvi-rename-bins to rename metagenomic bins with >60% completion or >2 Mbp in length and <10% redundancy values as metagenome-assembled genomes (MAGs).

```
for NAME in set.txt
do
    anvi-rename-bins -c 03_CONTIGS/$NAME-contigs.db \
                     -p 06_MERGED/$NAME/PROFILE.db \
                     --collection-to-read CONCOCT \
                     --collection-to-write FINAL_2 \
                     --call-MAGs \
                     --size-for-MAG 2 \
                     --min-completion-for-MAG 60 \
                     --max-redundancy-for-MAG 10 \
                     --prefix $NAME \
                     --report-file $NAME-renaming_bins.txt
done
```

We were unable to reconstruct bacterial MAG for Snake Pit using these criteria so the next steps were only done on the Rainbow and TAG sites and a new set file was used called `sets_final.txt`.

```
cat set_final.txt
RB
TAG
```

We used the program `anvi-summarize` to summarize the metagenomic binning results.

```
for NAME in `cat set_final.txt`
do
    anvi-summarize -c 03_CONTIGS/$NAME-contigs.db \
                   -p 06_MERGED/$NAME/PROFILE.db \
                   -C FINAL_2 \
                   -o 08_SUMMARY_FINAL_2/$NAME-SUMMARY
done
```

This program generates a directory containing for each bin a FASTA file and genomic features including completion and redundancy estimates. We used the program anvi-script-reformat-fasta to rename scaffolds contained in each MAG according to their coassembly group, and store their FASTA files in a separate directory called `09_REDUNDANT_MAGs_FINAL_2` for downstream analyses.

```
mkdir 09_REDUNDANT_MAGs_FINAL_2

for NAME in `cat set_final.txt`
do
    anvi-estimate-genome-completeness -c 03_CONTIGS/$NAME-contigs.db \
                                      -p 06_MERGED/$NAME/PROFILE.db \
                                      -C FINAL_2 | \
    grep MAG | \
    awk '{print $2}'> MAGs.txt

    # go through each MAG, in each SUMMARY directory, and store a
    # copy of the FASTA file with proper deflines in the 09_REDUNDANT_MAGs_FINAL_2
    # directory:
    for MAG in `cat MAGs.txt`
    do

        anvi-script-reformat-fasta 08_SUMMARY_FINAL_2/$NAME-SUMMARY/bin_by_bin/$MAG/$MAG-contigs.fa \
                                   --simplify-names \
                                   --prefix $MAG \
                                   -o 09_REDUNDANT_MAGs_FINAL_2/$MAG.fa
    done
done
```

For the identification and removal of redundant MAGs we prepared a `genomeinfo.csv` file with the MAG IDs and their redundancy and completion estimates.

```
echo -e "genome,completeness,contamination" > 09_REDUNDANT_MAGs_FINAL_2/genomeinfo.csv
for file in 08_SUMMARY_FINAL_2/*-SUMMARY/bins_summary.txt
do 
    tail -n +2 $file | awk '{print $1".fa,"$7","$8}' \
                     | grep '_MAG_00' >> 09_REDUNDANT_MAGs_FINAL_2/genomeinfo.csv
done
```

We used dRep to dereplicate our MAGs collection. We tried different coverage threshold to performe the dereplication from 0.10 to 0.75 and we obtained 49 MAGs with these differents coverage.
Here an example with the 0.75 coverage threshold.

```
dRep dereplicate 10_DREP_MAGs_FINAL_2 -p 12 -comp 10 -con 10 -sa 0.98 \
                 --cov_thresh 0.75 -g 09_REDUNDANT_MAGs_FINAL_2/*.fa \
                 --genomeInfo 09_REDUNDANT_MAGs_FINAL_2/genomeinfo.csv
```

To better estimate the abundance of each non-redundant MAG, we profiled once again all 49 MAGs in this final collection to avoid underestimating their abundance and detection across 4 metagenomes due to the competing reference contexts that redundant MAGs provided.

```
cat 10_DREP_MAGs_FINAL_2/dereplicated_genomes/*.fa >> 10_DREP_MAGs_FINAL_2/MAGs-contigs.fa

anvi-run-workflow -w metagenomics \
                  -c config-references-mode_MAGs.json \
                  --additional-params \
                  --cores 12 \
                  --keep-going --rerun-incomplete
```
The config file, fasta file and the samples files used in the workflow are available this way:

[config-references-mode_MAGs.json](files/config-references-mode_MAGs.json)

[samples_MG.txt](files/samples_MG.txt)

[fasta.txt](files/fasta.txt)

Successful completion of the anvi’o metagenomic workflow results in these five output directories: `00_MG_LOGS`, `03_MG_CONTIGS`, `04_MG_MAPPING`, `05_MG_ANVIO_PROFILE`, and `06_MG_MERGED`.


Although the anvi’o profile database in 06_MG_MERGED describes the distribution and detection statistics of all scaffolds in all MAGs, it does not contain a collection that describes the scaffold-bin affiliations. Thanks to our previous naming consistency, here we can generate a text file that describes these connections and import this collection into the anvi'o profile database
```
cd 03_MG_CONTIGS

mkdir -p collection

for split_name in `sqlite3 MAGs-contigs.db 'select split from splits_basic_info'`
do
    # in this loop $split_name goes through names like this: RB_MAG_00001_000000000001,
    # RB_MAG_00001_000000000002, RB_MAG_00001_000000000003 ...; so we can extract
    # the MAG name it belongs to:
    MAG=`echo $split_name | awk 'BEGIN{FS="_"}{print $1"_"$2"_"$3}'`
    
    # print it out with a TAB character
    echo -e "$split_name\t$MAG" >> collection/NR-MAGs-FINAL2-collection.txt
done

anvi-import-collection collection/NR-MAGs-FINAL2-collection.txt \
                       -c MAGs-contigs.db \
                       -p 06_MG_MERGED/MAGs/PROFILE.db \
                       -C NR_MAGs
```

### Investigating the MAGs functions

We carried out a global functional annotation with the KEGG database. We first exported all gene calls from the MAGs contigs database and removed the empty sequences

```
anvi-get-sequences-for-gene-calls -c 03_MG_CONTIGS/MAGs-contigs.db \
                                  --get-aa-sequences \
                                  -o 03_MG_CONTIGS/MAGs-protein-sequences.fa 
sed -i 's/>/>genecall_/' 03_MG_CONTIGS/MAGs-protein-sequences.fa
sed -i '/^>/ {N; /\n$/d}' 03_MG_CONTIGS/MAGs-protein-sequences.fa

```
We then ran the kofamscan annotation, parsed the result and imported them in anvi'o:

```
# Run kofamscan 
exec_annotation -f mapper \
                -o 03_MG_CONTIGS/MAGs-kegg_annotation.txt \
                03_MG_CONTIGS/MAGs-protein-sequences.fa

# Parsing the results from GhostKoala
python ../KEGG/GhostKoalaParser/KEGG-to-anvio --KeggDB ../KEGG/KO_Orthology_ko00001.txt \
                                              -i 03_MG_CONTIGS/MAGs-kegg_annotation.txt \
                                              -o 03_MG_CONTIGS/MAGs-KeggAnnotations-AnviImportable.txt

# Import kegg annotation in anvio
anvi-import-functions -c 03_MG_CONTIGS/MAGs-contigs.db \
                      -i 03_MG_CONTIGS/MAGs-KeggAnnotations-AnviImportable.txt
```
We used FeGenie to identify canonical iron genes with the `FeGenie.py` program:

```
mkdir -p 07_fegenie/GENE_CALLS

# Run fegenie
FeGenie.py -bin_dir 03_MG_CONTIGS/ --orfs -bin_ext fa -out 07_fegenie/fegenie_out -t 16 --meta

```

We parsed the result and imported them in anvi'o: 

```
# Parse and import gene function into anvi'o database
echo -e "gene_callers_id\tsource\taccession\tfunction\te_value" \
        >> 07_fegenie/fegenie_out/FeGenie-geneSummary_parsed.txt

tail -n +3 07_fegenie/fegenie_out/FeGenie-geneSummary.csv | while read line
do
    FUNCTION=`echo $line | awk -F"," '{print $4}'`
    GENE=`echo $line | awk -F"," '{print $3}' | sed -e 's/genecall_//g'`
    echo -e $GENE"\tFeGenie\t\t"$FUNCTION"\t0" >> 07_fegenie/fegenie_out/FeGenie-geneSummary_parsed.txt
done

anvi-import-functions -c 03_MG_CONTIGS/MAGs-contigs.db \
                      -i 07_fegenie/fegenie_out/FeGenie-geneSummary_parsed.txt

# Parse and import category into anvi'o database
echo -e "gene_callers_id\tsource\taccession\tfunction\te_value" \
        >> 07_fegenie/fegenie_out/FeGenie-geneSummary_category_parsed.txt
tail -n +3 07_fegenie/fegenie_out/FeGenie-geneSummary.csv | while read line
do
    FUNCTION=`echo $line | awk -F"," '{print $1}'` 
    GENE=`echo $line | awk -F"," '{print $3}' | sed -e 's/genecall_//g'`
    echo -e $GENE"\tFeGenie_category\t\t"$FUNCTION"\t0" \
            >> 07_fegenie/fegenie_out/FeGenie-geneSummary_category_parsed.txt
done

anvi-import-functions -c 03_MG_CONTIGS/MAGs-contigs.db \
                      -i 07_fegenie/fegenie_out/FeGenie-geneSummary_category_parsed.txt
```

Once we annotated our gene calls, we summarized our collections so we later have access to gene functions:


```
anvi-summarize -c 03_MG_CONTIGS/MAGs-contigs.db \
               -p 06_MG_MERGED/MAGs/PROFILE.db \
               -C NR_MAGs \
               -o 08_SUMMARY
```

### Taxonomical identification of MAGs

Taxonomic assignments of our MAGs was made with GTDB-Tk based on the Genome Database Taxonomy GTDB,  

```
mkdir -p 09_PHYLOGENO_GTDB/MAGs

for file in 08_SUMMARY/MAGs-SUMMARY/bin_by_bin/*/*-contigs.fa
do
    file2=$(basename $file | sed 's/-contigs.fa/.fna/g')
    cp $file 09_PHYLOGENO_GTDB/MAGs/$file2
done

gtdbtk classify_wf --cpus 33 \
                   --genome_dir 09_PHYLOGENO_GTDB/MAGs/ \
                   --out_dir 09_PHYLOGENO_GTDB/classify_wf_output
```

Closest GTDB references genomes were selected to perform an alignment of bacterial marker genes with our MAGs

```
gtdbtk align --identify_dir 09_PHYLOGENO_GTDB/classify_wf_output \
             --out_dir 09_PHYLOGENO_GTDB/align_output \
             --cpus 8 \
             --taxa_filter f__UBA1715,f__UBA3824,f__NBLH01,f__4572-128,c__Zetaproteobacteria,\
o__Thiotrichales,f__Wenzhouxiangellaceae,f__SZUA-36,f__Marinicellaceae,f__SZUA-38,f__SZUA-5,\
f__UBA2363,f__Ahniellaceae,g__Marinosulfonomonas,g__Amylibacter,g__UBA3077,g__UBA12010,\
g__Neptunicoccus,g__UBA5972,g__DSL-40,g__O448,g__Pontivivens,g__Albimonas,g__Oceanicella,\
g__Monaibacterium,g__Rubrimonas,c__Desulfobulbia,p__Myxococcota,f__Sulfurimonadaceae,\
f__Sulfurovaceae,c__Gracilibacteria,f__GCA-2747955,f__UBA1006,f__GCA-2747515,f__SW-6-46-9,\
f__UBA5272,f__UBA1568,p__Deinococcota,c__Kiritimatiellae,c__SZUA-567,c__UBA11346,f__Saprospiraceae,\
f__UBA10066,g__LPB0138,g__1-14-2-50-31-20,f__Melioribacteraceae,g__GCA-2747695,g__CG1-02-35-72,\
g__PDQE01,g__UBA7949,g__GCA-002401385,g__Lutibacter,g__Wenyingzhuangia,g__Ochrovirga,g__UBA7433,\
g__Tenacibaculum_A,g__Tenacibaculum,g__Polaribacter,g__Thermosediminibacter

```
The alignment was cleaned up by removing positions that were gap characters in more than 50% of the sequences using trimAL

```
mkdir -p 09_PHYLOGENO_GTDB/anvio_tree

# Make tree with the MSA file from gtdb_tk
trimal -in 09_PHYLOGENO_GTDB/align_output/gtdbtk.bac120.msa.fasta \
       -out 09_PHYLOGENO_GTDB/anvio_tree/gtdbtk_bac120_msa_anvio_clean.fasta \
       -gt 0.50

```
And we built a maximum likelihood tree using IQ-TREE with the 'WAG' general matrice model.

```
iqtree -s 09_PHYLOGENO_GTDB/anvio_tree/gtdbtk_bac120_msa_anvio_clean.fasta \
       -nt 8 \
       -m WAG \
       -bb 1000

```
Now we have a newick tree that shows our MAG with the closest GTDB genomes. We used the anvi’o interactive interface to visualize this new tree:

```
anvi-interactive -p 09_PHYLOGENO_GTDB/anvio_tree/phylogenomic-profile.db \
                 -t 09_PHYLOGENO_GTDB/anvio_tree/gtdbtk_bac120_msa_anvio_clean.fasta.contree \
                 --title "Phylogenomics Tree" \
                 --manual
```

![tree step1](files/Figure4_step1.png)

To improve the visualization, we prepared a collection file that describes the sources of the genomes and we imported it for the anvi'o interactive interface

```
grep "^>" 09_PHYLOGENO_GTDB/align_output/gtdbtk.bac120.msa.fasta \
          | awk -F" " '{print $1}' | sed 's/>//g' | while read NAME
do
    if [[ "$NAME" == *MAG* ]]
    then echo -e $NAME"\tMAG" >> 09_PHYLOGENO_GTDB/anvio_tree/source_collection.txt
    else echo -e $NAME"\tGTDB" >> 09_PHYLOGENO_GTDB/anvio_tree/source_collection.txt
    fi
done

```
The following output provides a glimpse from the file `source_collection.txt`:

```
head 09_PHYLOGENO_GTDB/anvio_tree/source_collection.txt
GB_GCA_000186885.1	GTDB
GB_GCA_000260135.1	GTDB
GB_GCA_000310245.1	GTDB
GB_GCA_000379225.1	GTDB
GB_GCA_000503875.1	GTDB
GB_GCA_000737335.3	GTDB
GB_GCA_000747095.1	GTDB
GB_GCA_000769715.1	GTDB
GB_GCA_000830175.1	GTDB
GB_GCA_000830255.1	GTDB

```

We used the program anvi-import-collection to import this information back into the anvi’o profile databases.

```
anvi-import-collection 09_PHYLOGENO_GTDB/anvio_tree/source_collection.txt \
                       -p 09_PHYLOGENO_GTDB/anvio_tree/phylogenomic-profile.db \
                       -C Source
```

We prepared a matrix to add phylum level information for all the tree branches and family or genus level if it's identical to the taxonomy of our MAGs

```
echo -e "genome_id\tPhyla\tFamilies\tGenera" > 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt

# Add the taxonomy of our MAGs
tail -n +2 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv | while read line
do
    NAME=`echo $line | awk -F" |;" '{print $1}'`
    PHYLUM=`echo $line | awk -F" |;" '{print $3}' | sed 's/.__//g'`
    FAMILY=`echo $line | awk -F" |;" '{print $6}' | sed 's/.__//g'`
    GENUS=`echo $line | awk -F" |;" '{print $7}' | sed 's/.__//g'`
    echo -e $NAME"\t"$PHYLUM"\t"$FAMILY"\t"$GENUS >> 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt
done

# Prepare lists with MAGs taxonomy at the genus and family level
awk -F";" '{print $5}' 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv \
    | sed 's/.__//g' | sed '/^$/d' | uniq | tr -d '\n' >> 09_PHYLOGENO_GTDB/anvio_tree/taxa_to_keep.txt
awk -F";" '{print $6}' 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv \
    | sed 's/.__//g' | sed '/^$/d' | uniq | tr -d '\n' >> 09_PHYLOGENO_GTDB/anvio_tree/taxa_to_keep.txt

# Add the taxonomy of the GTDB genomes at the phylum level and at the genus or family level if it is identical
# to the taxonomy of our MAGs
grep "^>" 09_PHYLOGENO_GTDB/align_output/gtdbtk.bac120.msa.fasta | sed 's/>//g' | while read line
do
    for TAX_MAG in `cat 09_PHYLOGENO_GTDB/anvio_tree/taxa_to_keep.txt`
    do
        NAME=`echo $line | awk -F" |;" '{print $1}'`
        PHYLUM=`echo $line | awk -F" |;" '{print $3}' | sed 's/.__//g'`    
        FAMILY=`echo $line | awk -F" |;" '{print $6}' | sed 's/.__//g'`
        GENUS=`echo $line | awk -F" |;" '{print $7}' | sed 's/.__//g'`
        if [[ "$NAME" == *MAG* ]]
        then 
            continue 
        elif [[ "$TAX_MAG" =~ "$FAMILY" ]] && [[ "$TAX_MAG" =~ "$GENUS" ]]
        then
            echo -e $NAME"\t"$PHYLUM"\t"$FAMILY"\t"$GENUS >> 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt
        elif [[ "$TAX_MAG" =~ "$FAMILY" ]]
        then
            echo -e $NAME"\t"$PHYLUM"\t"$FAMILY"\t" >> 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt
        else
            echo -e $NAME"\t"$PHYLUM"\t\t" >> 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt
    fi
    done
done
```
The following output provides a glimpse from the file `view_data.txt`:

```
head 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt 
genome_id	Phyla	Families	Genera
RB_MAG_00024	Patescibacteria	UBA1006	UBA1006
RB_MAG_00013	Patescibacteria	UBA1006	UBA1006
RB_MAG_00022	Patescibacteria	GCA-2747955	
TAG_MAG_00007	Patescibacteria	GCA-2747515	
RB_MAG_00020	Patescibacteria	UBA4473	
RB_MAG_00019	Patescibacteria		
TAG_MAG_00020	Patescibacteria		
TAG_MAG_00018	Patescibacteria		
RB_MAG_00027	Patescibacteria	UBA6164	
```

Finally used the program anvi-misc-data to import this information back into the anvi’o profile databases.

```
anvi-import-misc-data 09_PHYLOGENO_GTDB/anvio_tree/view_data.txt \
                      -p 09_PHYLOGENO_GTDB/anvio_tree/phylogenomic-profile.db \
                      --target-data-table items
```
We visualized our tree in the interactive interface of anvio

```
anvi-interactive -p 09_PHYLOGENO_GTDB/anvio_tree/phylogenomic-profile.db \
                 -t 09_PHYLOGENO_GTDB/anvio_tree/gtdbtk_bac120_msa_anvio_clean.fasta.contree \
                 --title "Phylogenomics Tree" \
                 --manual
```
We then manually rerooted the tree with the genome RS_GCF_000144645.1 related to Firmicutes phyla, we loaded the `Source` collection and changed the colors and size of the layers. Then Inkscape was used to achieve this display:

![Maximum likelihood tree](files/Figure4.png)

### Generate anvi'o static image

For the anvi'o static image of the MAG we built a phylogenomic tree including only our MAG:

```
mkdir -p 09_PHYLOGENO_GTDB/anvio_tree_MAG

trimal -in 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.user_msa.fasta \
       -out 09_PHYLOGENO_GTDB/anvio_tree_MAG/gtdbtk_bac120.user_msa_anvio_clean.fasta \
       -gt 0.50 

iqtree -s 09_PHYLOGENO_GTDB/anvio_tree_MAG/gtdbtk_bac120.user_msa_anvio_clean.fasta \
       -nt 8 \
       -m WAG \
       -bb 1000
```
Now we have a newick tree that shows our MAG. We used the anvi’o interactive interface to visualize this new tree:

```
anvi-interactive -t 09_PHYLOGENO_GTDB/anvio_tree_MAG/gtdbtk_bac120.user_msa_anvio_clean.fasta.contree \
                 -p 12_TABLES/profile.db \
                 --title "anvi'o static image" \
                 --manual
```

![Static image](files/Anvio_static_image_step1.png)

We used the program `anvi-estimate-genome-completeness` to export the estimate completion, redundancy and the genome length obtained using anvio

```
anvi-estimate-genome-completeness -c 03_MG_CONTIGS/MAGs-contigs.db \
                                  -p 06_MG_MERGED/MAGs/PROFILE.db \
                                  -C NR_MAGs \
                                  -o 08_SUMMARY/MAGs-genome-completeness.txt

echo -e "Completion\tRedundancy\tLength (Mbp)\tMAG name" > 12_TABLES/Completion.txt
tail -n +2 08_SUMMARY/MAGs-genome-completeness.txt \
    | awk -F"\t" '{print $4"\t"$5"\t"$7"\t"$1}' >> 12_TABLES/Completion.txt
```
We retrived the class annotation of the MAG from GTDB_Tk classify workflow and produceed a table with the GC content, length, completion, rendundancy and mean coverage obtained with anvio

```
echo -e "Class" > 12_TABLES/Class.txt
tail -n +2 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv \
    | sort | awk -F";|\t" '{print $4}' | sed -e "s/.__//g" >> 12_TABLES/Class.txt

echo -e "GC content" > 12_TABLES/GC.txt
for GC in `cat 08_SUMMARY/MAGs-SUMMARY/bin_by_bin/*/*-GC_content.txt`
do
    echo -e $GC >> 12_TABLES/GC.txt
done

paste 08_SUMMARY/MAGs-SUMMARY/bins_across_samples/mean_coverage.txt 12_TABLES/GC.txt \
      12_TABLES/Completion.txt 12_TABLES/Class.txt > 12_TABLES/items-data.txt

```

With R we also produced a data matrix representing the percent of reads recruited to the bins for each sample at the family level. To generate this matrix we used an output from the `anvi-summarize` and `gtdbtk classify_wf`:

```
Tax <- read.csv("09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv", sep="\t", dec = ".")
Pct_recr <- read.csv("08_SUMMARY/MAGs-SUMMARY/bins_across_samples/bins_percent_recruitment.txt", 
                     sep="\t", dec = ".", header = FALSE, row.names = 1)

library(dplyr)
library(tidyverse)

Pct_recr_t <- as.data.frame(t(Pct_recr))
Pct_recr_t <- Pct_recr_t %>% 
  filter(samples != "__splits_not_binned__") %>%
  rename(MAGs = samples) 

Classification <- Tax %>%
  select(user_genome, classification) %>% 
  separate(classification, c("domain", "kingdom", "class", "order", "family", "genus", "species"), 
           sep = ";") %>%
  rename(MAGs = user_genome) %>%
  select(MAGs, family) %>%
  right_join(Pct_recr_t, by="MAGs") %>%
  mutate_at(vars(F_Rainbow:M_TAG), as.character) %>%
  mutate_at(vars(F_Rainbow:M_TAG), as.numeric) %>%
  group_by(family) %>%
  summarise_at(vars(F_Rainbow:M_TAG), funs(sum)) %>%
  mutate(family = str_replace(family, "f__", "families!"))

Classification_sum <- Classification %>%
  mutate(Sum = F_Rainbow + F_SnakePit + F_TAG + M_Rainbow + M_SnakePit + M_TAG)

Other <- Classification_sum[Classification_sum$Sum <= 3,]$family

Classification_sum[Classification_sum$family %in% Other,]$family <- 'families!Other'

Classification_sum2 <- Classification_sum %>%
  group_by(family) %>%
  summarise_at(vars(F_Rainbow:M_TAG), funs(sum)) %>%
  rename(samples = family)
 
Classification_sum2$samples[Classification_sum2$samples == "families!"] <- "families!NA"

Classification_t <- as.data.frame(t(Classification_sum2))

write.table(Classification_t, file = "12_TABLES/family.txt", 
            sep = "\t", col.names = FALSE, quote = FALSE)
```
We completed the default table from the `anvi-summarize` command output with this family table 

```
cut -f 2- 08_SUMMARY/MAGs-SUMMARY/misc_data_layers/default.txt | paste 12_TABLES/family.txt - \
          >> 12_TABLES/layers-data.txt

```
We used the program anvi-misc-data to import these informations back into the anvi’o profile databases.

```
anvi-import-misc-data 12_TABLES/items-data.txt \
                      -p 12_TABLES/profile.db \
                      --target-data-table items

anvi-import-misc-data -p 12_TABLES/profile.db \
                      12_TABLES/layers-data.txt \
                      --target-data-table layers
```
We visualized our tree and tables in the interactive interface of anvio

```
anvi-interactive -t 09_PHYLOGENO_GTDB/anvio_tree_MAG/gtdbtk_bac120.user_msa_anvio_clean.fasta.contree \
                 --title "anvi'o static image" \
                 -p 12_TABLES/profile.db \
                 --manual

```
We then manually organized the order of items with the MAGs phylogenomic tree. We changed the colors and size of the layers. Then Inkscape was used to achieve this display:

![Static image](files/Anvio_static_image_step2.png)

### MAGs pathways visualisation 

To visualise the metabolic pathways and their completion level we used KEGG_DECODER

We exported the taxonomical information of our MAGs obtained with GTDB_TK to add it in the name of the MAG using this command:

```
mkdir -p 11_KEGG_DECODER

tail -n +2 09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv | while read line
do
    FAMILY=`echo $line | awk -F" |;" '{print $6}' | sed -e 's/f__//g'`
    ORDER=`echo $line | awk -F" |;" '{print $5}' | sed -e 's/o__//g'`
    MAG=`echo $line | awk -F" |;" '{print $1}'`
if
    [ -z "$FAMILY" ]
then
    echo -e $MAG"-Unclassified-"$ORDER >> 11_KEGG_DECODER/MAGs-taxonomy.txt
else
    echo -e $MAG"-"$FAMILY >> 11_KEGG_DECODER/MAGs-taxonomy.txt
fi
done

```
The following output provides a glimpse from the file `MAGs-taxonomy.txt`:

```
head 11_KEGG_DECODER/MAGs-taxonomy.txt
RB_MAG_00024-UBA1006
RB_MAG_00013-UBA1006
RB_MAG_00022-GCA-2747955
TAG_MAG_00007-GCA-2747515
RB_MAG_00020-UBA4473
RB_MAG_00019-Unclassified-GCA-2401425
TAG_MAG_00020-Unclassified-GCA-2401425
TAG_MAG_00018-Unclassified-GCA-2401425
RB_MAG_00027-UBA6164
RB_MAG_00017-UBA6164
```

We retrieved the KEGG Orthology of the genes from the `anvi-summarize` program to built the matrice input for KEGG DECODER

```

for line in `cat  11_KEGG_DECODER/MAGs-taxonomy.txt`
do
    MAGs=$(echo $line | awk 'BEGIN{FS="-"}{print $1}')
    TAX="${line/_MAG_/-MAG-}"
    tail -n +2 08_SUMMARY/bin_by_bin/$MAGs/$MAGs-gene_calls.txt \
         | awk 'BEGIN{FS="\t"}{print "'$TAX'""_"$1"\t"$11}' \
         >> 11_KEGG_DECODER/keggdecoder_input.txt
done

```

The following output provides a glimpse from the file `keggdecoder_input.txt`:

```
head 11_KEGG_DECODER/keggdecoder_input.txt
RB-MAG-00024-UBA1006_45745	K00560
RB-MAG-00024-UBA1006_45746	
RB-MAG-00024-UBA1006_45747	K07260
RB-MAG-00024-UBA1006_45748	K00600
RB-MAG-00024-UBA1006_45749	K07082
RB-MAG-00024-UBA1006_45750	K02342
RB-MAG-00024-UBA1006_45751	
RB-MAG-00024-UBA1006_45752	K00566
RB-MAG-00024-UBA1006_45753	K07507
RB-MAG-00024-UBA1006_45754	K10857

```
We ran the following command to generate the matrice heatmap:

```
KEGG-decoder -i 11_KEGG_DECODER/keggdecoder_input.txt \
             -o 11_KEGG_DECODER/keggdecoder_out.list -v static

```
We sorted the KEGG_DECODER output and transposed it with datamash,

```
sort 11_KEGG_DECODER/keggdecoder_out.list | datamash transpose > 11_KEGG_DECODER/kegg_decoder_sorted_t.txt
```
We also manually defined group in order to gather the metabolic pathway. The modified `kegg_decoder_sorted_t.txt` file is available this way:

[kegg_decoder_sorted_t.txt](files/kegg_decoder_sorted_t.txt)

And we produced the heatmap using R, we removed the low completion pathways. We ordered the MAGs on the heatmap based on their euclidean distances and indicated the completion, class and phylum.

```
library(ComplexHeatmap)
library(circlize)
library(RColorBrewer)
library(dplyr)

# Import KEGG pathways table as well as taxonomy obtained with GTDB_TK 
# and the completeness obtained from anvio
KEGG_t <- read.csv("11_KEGG_DECODER/kegg_decoder_sorted_t.txt", sep="\t", dec = ".", row.names = 1)
Tax <- read.csv("09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv", sep = "\t", dec = ".", row.names = 1 )
Completeness <- read.csv("08_SUMMARY/MAGs-genome-completeness.txt", sep = "\t", dec =".", row.names = 1)

# Extract and sort taxonomy
Tax$classification <- gsub(".__", "", Tax$classification)
Tax <- Tax %>%
  select(classification) %>%
  separate(classification, c("kingdom","phylum","class","order","family","genus","specie"), sep = ";")
Tax_sort <- Tax[ order(row.names(Tax)), ]

# Select only completeness column
Completeness <- Completeness %>%
  select(X..completion) %>%
  rename(Completion = X..completion)

# Merge taxonomy and completeness into a single table
Tax_sort <-  merge(Tax_sort, Completeness, by=0)
rownames(Tax_sort) <- Tax_sort$Row.names
Tax_sort$Row.names <- NULL

# Remove low completion pathways
KEGG_group <- KEGG_t %>%
  select(Group)
KEGG_num_filter <- KEGG_t[, grep("MAG.00", colnames(KEGG_t))] %>%
  filter(apply(., 1, function(x) any(x > 0.3)))
KEGG_num_filter_group <-  merge(KEGG_num_filter, KEGG_group, by=0)
rownames(KEGG_num_filter_group) <- KEGG_num_filter_group$Row.names
KEGG_num_filter_group$Row.names <- NULL

# Remove undiscussed metabolic pathways
KEGG_subset <- KEGG_num_filter_group %>%
  filter(!Group %in% c("C-P lyase", "Anaplerotic Reactions", 
                       "Mixed Acid Fermentation", "Competence−related DNA transporter", 
                       "Methane metabolism"))

KEGG_s = as.matrix(KEGG_subset[, grep("MAG.00", colnames(KEGG_subset))])

hm.palette <- colorRampPalette(rev(brewer.pal(9, 'YlOrRd')), space='Lab')


c = Tax_sort$class
p = Tax_sort$phylum
comp = Tax_sort$Completion

group_order_s <- factor(KEGG_subset$Group, levels=c("Carbon fixation", "Carbohydrate metabolism", 
                                                    "Carbon degradation", "Nitrogen metabolism", 
                                                    "Sulfur metabolism",  "Oxidative phosphorylation", 
                                                    "Hydrogen redox", "Amino acid metabolism", 
                                                    "Vitamins biosynthesis", "Cell motility", 
                                                    "Biofilm formation", "Bacterial Secretion Systems", 
                                                    "Transporters", "Metal Transporters", 
                                                    "Arsenic reduction"))

colourCount_c = length(unique(c))
getPalette_c = colorRampPalette(brewer.pal(8, "Dark2"))
c_col = structure(getPalette_c(colourCount_c), names = unique(c))

col_comp = colorRamp2(c(40, 70, 100), c("grey80", "grey40", "grey0"))

ht_opt(legend_title_gp = gpar(fontsize = 7, fontface = "bold"), 
       legend_labels_gp = gpar(fontsize = 7))

taxo = HeatmapAnnotation(Phylum = p, Class = c, Completion = comp,
                         annotation_name_side = "left", na_col = "white",
                         annotation_name_gp = gpar(fontsize = c(6)),
                         annotation_legend_param = list(Completion = list(title = "MAG completion")),
                         col = list(Phylum = c("Bacteroidota" = "#638bcb", "Campylobacterota" = "#4aac8d",
                                               "Desulfobacterota" = "#7964ce", "Proteobacteria" = "#64ac48",
                                               "Verrucomicrobiota" = "#cf5568", "Patescibacteria" = "#d38cca",
                                               "Myxococcota" = "#c950b5", "Planctomycetota" = "#4aac8d", 
                                               "Deinococcota" = "#a39440"),
                                    Class = structure(names = c("Bacteroidia", "Campylobacteria", "Ignavibacteria", 
                                                                "Desulfobulbia", "Zetaproteobacteria", "Kiritimatiellae",
                                                                "Paceibacteria", "Alphaproteobacteria", "Gracilibacteria",
                                                                "Gammaproteobacteria", "Bradimonadia", "SZUA-567",
                                                                "Paceibacteria_A", "Deinococci"), getPalette_c(colourCount_c)),                                    
                                    Completion = col_comp))

annotation_titles = c(Completion = "MAG completion")

ht_kegg_s = Heatmap(KEGG_s, name="Pathways completeness", cluster_rows = FALSE, 
                    rect_gp = gpar(col = "black", lwd = 0.5), 
                    col=colorRamp2(c(0, 0.125, 0.250, 0.375, 0.5, 0.625, 0.75, 0.875, 1), 
                                   brewer.pal(n=9, name="YlOrRd")),
                    top_annotation = taxo,
                    row_split = group_order_s, row_title_rot = 0, 
                    row_title_gp = gpar(col = c("black"), fontsize = c(7)), 
                    row_names_gp = gpar(col = c("black"), fontsize = c(7)),
                    clustering_distance_columns = "euclidean",
                    column_names_gp = gpar(col = c("black"), fontsize = c(6)))

draw(ht_kegg_s, merge_legends = TRUE)

```

![KEGG_DECODER Heatmap](files/Rplot_KEGG_Decoder.png)


### Differentially Abundant MAGs between site

To identify differentially abundant MAGs between sites we retrieved the raw counts mapping for each MAG from the .bam file using samtools view.

```
mkdir 13_DESeq2

# Get the list of the contigs for each MAG
for file in 08_SUMMARY/MAGs-SUMMARY/bin_by_bin/*/*-contigs.fa
do
    cat $file | grep '^>' | sed 's/>//' | sed ':a;N;$!ba;s/\n/ /g' >> 13_DESeq2/list_contigs_MAGs.txt
done

# Run samtools view for each contigs of MAGs
cat 13_DESeq2/list_contigs_MAGs.txt | while read CONTIGS
do
    for BAM in 04_MG_MAPPING/MAGs/*.bam
    do
        SAMPLE="$(basename $BAM | sed 's/.bam//g')"
        MAG=`echo $CONTIGS | cut -f1 -d" " | cut -f -3 -d"_"`
        COUNT=`samtools view -c $BAM $CONTIGS`
        echo -e $SAMPLE"\t"$MAG"\t"$COUNT >> 13_DESeq2/samtools.txt
    done
done
```
With R we imported the number of aligned reads obtained from samtools view.

```
# load library
library(dplyr)
library(tidyr)
library(DESeq2)
library(ggplot2)
library(stringr)
library(tidyverse)
library(edgeR)

# load raw reads dataset
data <- read.table("13_DESeq2/samtools.txt", sep="\t")
count <- data %>% 
  dplyr::rename(Sample = V1, MAG = V2, Raw_reads = V3) %>% 
  spread(Sample, Raw_reads)
```

We imported the MAGs length from the `MAGs-genome-completeness.txt` file and we normalized mapping data within and between samples with the Gene length corrected Trimmed Mean of M-values (GeTMM) method using genome length instead of gene length.

```
# Import `MAGs-genome-completeness.txt` file
completeness <- read.table("08_SUMMARY/MAGs-genome-completeness.txt", sep="\t", header = TRUE)

# Create dataframe with genome length
length <- completeness %>%
  select(bin.name, total.length) %>%
  dplyr::rename(MAG = bin.name, length = total.length) %>%
  inner_join(count, by="MAG") %>%
  column_to_rownames(var = "MAG")

# Calculate reads per Kbp of MAG length (MAG length is in bp and converted to Kbp)
rpk <- (length[,2:ncol(length)]*10^3/length[,1])

# Compare group
group <- c(rep("A",ncol(rpk)))
y <- DGEList(counts=rpk, group=group)

# Normalize for library size by calculating scaling factor using TMM
y <- calcNormFactors(y)

# count per million read (normalized count)
norm_counts <- round(as.data.frame(cpm(y)))

# Export normalized count table
norm_counts <- norm_counts %>% rownames_to_column(var = "rowname") %>%
   dplyr::rename(MAG = rowname)
write.table(norm_counts, file="13_DESeq2/GeTMM_count.txt", sep = "\t", row.names = FALSE, quote = FALSE)
```

We used DESeq2 for differential MAG abundance analysis between sites.

```
# Create metadata table
metadata <- data.frame(Sample = c("RE5", "RE6", "RE12", "RE13"),
                 Site = c("TAG", "TAG", "Rainbow", "Rainbow"))

# Construct DESeq dataset object
dds <- DESeqDataSetFromMatrix(countData=norm_counts, colData=metadata, design=~Site, tidy = TRUE)

#  Run DESeq function
dds <- DESeq(dds)
  dplyr::rename(Sample = V1, MAG = V2, Raw_reads = V3) %>%
  spread(Sample, Raw_reads)

# Create taxonomy table with gtdb_tk classification
Tax <- read.csv("09_PHYLOGENO_GTDB/classify_wf_output/gtdbtk.bac120.summary.tsv", sep="\t", dec = ".")
Classification <- Tax %>%
  select(user_genome, classification) %>%
  separate(classification, c("Domain", "Phylum", "Class", "Order", "Family", "Genus", "Species"), sep = ";") %>%
  dplyr::rename(MAG = user_genome) %>%
  column_to_rownames(var="MAG")

# Export DESeq result table
res = results(dds, cooksCutoff = FALSE)
DeSeq_table <- as.data.frame(cbind(as(res, "data.frame"), as(Classification[rownames(res), ], "matrix")))
DeSeq_table <- DeSeq_table %>%
  rownames_to_column() %>%
  dplyr::rename(MAG = rowname)
write.table(DeSeq_table, file="Additional_file_5.txt", row.names = FALSE, sep = "\t", quote = FALSE)
```

Differentially abundant MAGs with adjusted p-value of 0.01 and absolute log2 fold change of 2 were regarded as significant in this study. We identified 35 MAGs that were significantly differentially abundant between sites and we used a scatterplot to represent them.

```
# Select significantly differentially abundant MAGs
sigtab = res[which(abs(res$log2FoldChange) > 2 & res$padj < 0.01), ]
sigtab = cbind(as(sigtab, "data.frame"), as(Classification[rownames(sigtab), ], "matrix"))
sigtab$MAG <- rownames(sigtab)

# Create table to plot
sigtab_p <- sigtab %>% 
  mutate(Phylum = str_replace(Phylum, "p__", "")) %>%
  mutate(Family = str_replace(Family, "f__", "")) %>%
  mutate(MAG_Family = paste(Family, MAG, sep=" "))

# Order MAGs by log2FoldChange
x = tapply(sigtab_p$log2FoldChange, sigtab_p$MAG_Family, function(x) max(x))
x = sort(x, TRUE)
sigtab_p$MAG_Family = factor(as.character(sigtab_p$MAG_Family), levels=names(x))

# Plot MAGs
ggplot(sigtab_p, aes(log2FoldChange, MAG_Family, size=baseMean, color=Phylum)) +
  geom_point() +
  theme_bw() + 
  xlab("log2 Fold Change") + 
  ylab("MAG") + 
  labs(colour="Phylum", size="Mean counts") +
  scale_size_continuous(labels=c("1,000","10,000","100,000"), breaks = c(1000, 10000, 100000)) +
  scale_color_manual(values = c("Bacteroidota" = "#638bcbff", "Campylobacterota" = "#4aac8dff", 
                                "Deinococcota" = "#a39440ff", "Desulfobacterota" = "#7964ceff", 
                                "Myxococcota" = "#c950b5ff", "Patescibacteria" = "#d38ccaff", 
                                "Planctomycetota" = "#4aac8dff", "Proteobacteria" = "#64ac48ff"))
```

![DESeq2](files/Additional_file_6.png)

### Taxonomic composition of samples based on the small-subunit rRNA

To explore the taxonomic composition of the community we used phyloFlash (v3.4) together with SILVA database (release 138.1) on the quality-filtered short metagenomic reads.

```
mkdir phyloflash
cd phyloflash

for r1 in 01_QC/RE*_R1.fastq.gz
do
# copy the filename, r1, to a new file name, r2 and substitute R1 in the name with R2
# this generates the name of the R2 file
    r2=$r1
    r2="${r1/_R1/_R2}"
# generate the names for the output files
    NAME=$(basename $r1 | sed 's/-QUALITY_PASSED_R1.fastq.gz//g')
# run the phyloFlash command
    phyloFlash.pl -lib $NAME -almosteverything \
                  -log -read1 $r1 -read2 $r2 -CPUs 56 \
                  -dbhome /home/datawork-lmee-intranet-nos/database/phyloflash/138.1
done
```

We then compare the results with the `phyloFlash_compare.pl` command:

```
phyloFlash_compare.pl --allzip --task heatmap --log --out RE --outfmt png
```

![phyloFlash](files/RE.phyloFlash_compare.heatmap.png)
